package com.company;

import java.util.*;

public class Lexer {
    static class Token {
        enum TokenType {
            NUM,
            OR,
            AND,
            ERR
        }

        private String string;
        private TokenType tokenType;

        public Token(String string, TokenType tokenType) {
            this.string = string;
            this.tokenType = tokenType;
        }

        public String getString() {
            return string;
        }

        public TokenType getTokenType() {
            return tokenType;
        }
    }

    private HashMap<String, Token.TokenType> lookUpTokenType;
    private StringTokenizer tokenizer;
    private List<Token> tokens;

    public Lexer(String expression) {
        tokenizer = new StringTokenizer(expression, " ");
        tokens = new ArrayList<>();

        lookUpTokenType = new HashMap<>();
        lookUpTokenType.put("^", Token.TokenType.OR);
        lookUpTokenType.put("&", Token.TokenType.AND);

        processExpression();
    }

    private void processExpression() {
        Token lastToken = null;

        while ( tokenizer.hasMoreElements() ) {
            String str = tokenizer.nextToken();

            if ( lookUpTokenType.containsKey( str ) ) {
                lastToken = new Token(str, lookUpTokenType.get(str));
                tokens.add(lastToken);
                continue;
            }

            try {
                Long.parseLong(str);
                lastToken = new Token(str, Token.TokenType.NUM);
                tokens.add(lastToken);
            } catch ( Exception e) {
                lastToken = new Token("", Token.TokenType.ERR);
                tokens.add(lastToken);
                break;
            }
        }
    }

    public Enumeration<Token> elements() {
        return new Enumeration<Token>() {
            private int current = 0;

            @Override
            public boolean hasMoreElements() {
                return current < tokens.size();
            }

            @Override
            public Token nextElement() {
                return tokens.get(current++);
            }
        };
    }

    public Iterator<Token> iterator() {
        return tokens.iterator();
    }
}
