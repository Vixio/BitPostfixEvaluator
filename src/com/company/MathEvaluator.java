package com.company;

import java.util.BitSet;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.Stack;
import java.util.function.BinaryOperator;
import com.company.Lexer.Token;

public class MathEvaluator {

    public BitSet evaluate(String expression) {
        Lexer lexer = new Lexer(expression);

        EnumMap<Token.TokenType, BinaryOperator<BitSet> > map = new EnumMap<>(Token.TokenType.class);
        map.put(Token.TokenType.AND, this::and);
        map.put(Token.TokenType.OR, this::or);

        Stack<BitSet> stack = new Stack<>();

        Enumeration<Lexer.Token> elems = lexer.elements();
        while( elems.hasMoreElements() ) {
            Lexer.Token token = elems.nextElement();

            if ( token.getTokenType() == Token.TokenType.ERR ) {
                return null;
            }
            else if ( token.getTokenType() == Token.TokenType.NUM ) {
                stack.push(BitSet.valueOf(new long[] { Long.parseLong(token.getString())}));
            }
            else {
                if ( stack.size() < 2 ) {
                    return null;
                }

                BitSet n2 = stack.pop();
                BitSet n1 = stack.pop();

                BitSet output = map.get(token.getTokenType()).apply(n1, n2);
                stack.push(output);
            }
        }

        if ( stack.size() != 1 ) {
            return null;
        }

        return stack.pop();
    }

    private BitSet and(BitSet n1, BitSet n2) {
        BitSet bs = (BitSet)n1.clone();
        bs.and(n2);
        return bs;
    }

    private BitSet or(BitSet n1, BitSet n2) {
        BitSet bs = (BitSet)n1.clone();
        bs.or(n2);
        return bs;
    }
}
