package com.company;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Scanner;

//
// StringTokenizer
// HashTable
// EnumMap
// BitSet
// Stack
// Enumeration
// Iterator
//

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese una expresion booleana posfija:");
        System.out.println("Ejemplo: 1 2 | 4 &");
        System.out.println("Simbolos & (AND), ^ (OR):");
        String line = scanner.nextLine();

        MathEvaluator evaluator = new MathEvaluator();
        BitSet output = evaluator.evaluate(line);

        if ( output != null ) {
            System.out.println("Resultado: " + output.toLongArray()[0]);
        } else {
            System.out.println("Expresion no valida");
        }
    }
}
